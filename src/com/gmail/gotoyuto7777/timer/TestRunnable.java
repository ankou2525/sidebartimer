package com.gmail.gotoyuto7777.timer;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;

public 	class TestRunnable extends BukkitRunnable {

	static HashMap<String,Integer> timer2 = new HashMap<String,Integer>();
	public void run() {

		timer2.put("タイマ", timer2.get("タイマ")-1);
		ScoreboardManager manager = Bukkit.getScoreboardManager();
		Scoreboard board = manager.getMainScoreboard();
		Objective objective = board.getObjective("timer");
		Score score = objective.getScore(Bukkit.getOfflinePlayer(ChatColor.GREEN + "時間:"+ ChatColor.YELLOW));
		score.setScore(timer2.get("タイマ"));

		if(timer2.get("タイマ")==0){
			objective.unregister();
			cancel();
		}
	}
}