package com.gmail.gotoyuto7777.timer;

import java.util.HashMap;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;

public class Timer extends JavaPlugin {
	static HashMap<String,Integer> time = new HashMap<String,Integer>();
	String pf = ChatColor.GREEN+"[SBTimer] "+ChatColor.AQUA;
	ChatColor aqua = ChatColor.AQUA;
	int task;

	Logger log;

	public void onEnable(){
		log = this.getLogger();
		this.saveDefaultConfig();
		log.info("SideBarTimer有効になりました。");
		time.put("タイム", 0);
	}
	public void onDisable(){
		log.info("SideBarTimer無効になりました。");
	}
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(cmd.getName().equalsIgnoreCase("sbt")){
			if(!sender.hasPermission("sidebartimer.allow")){
				sender.sendMessage(pf + ChatColor.DARK_RED +"あなたはこのコマンドを使用する権限がありません！");
				return false;
			}
			if(sender.hasPermission("sidebartimer.allow")){
				if(args.length<=0){
					sender.sendMessage(pf + ChatColor.RED +"コマンドに入力されていないところがあります。");
					sender.sendMessage(pf + ChatColor.WHITE +"/sbt <start,stop,help,reload,tpset>");
					return false;
				}
				if(args[0].equalsIgnoreCase("tpset")){
					Player p = (Player) sender;
					Location loc = p.getLocation();
					int x = (int) loc.getX();
					int y = (int) loc.getY();
					int z = (int) loc.getZ();
					getConfig().set("tppoint.x", x);
					getConfig().set("tppoint.y", y);
					getConfig().set("tppoint.z", z);
					this.saveConfig();
					p.sendMessage(pf + "TPポイントを設定しました。");
					p.sendMessage(pf + "設定座標： x"+ x +" y"+ y +" z"+ z);
				}
				if(args[0].equalsIgnoreCase("reload")){
					this.reloadConfig();
					sender.sendMessage(pf + "SideBarTimerのconfigをリロードしました。");
				}
				if(args[0].equalsIgnoreCase("help")){
					Player p = (Player) sender;
					p.sendMessage(pf + ChatColor.GREEN +"*========== SideBarTimer<1/1> ==========*");
					p.sendMessage(pf + ChatColor.GREEN +"/sbt start <時間> : <時間>の数でタイマーを開始します。");
					p.sendMessage(pf + ChatColor.GREEN +"/sbt stop : タイマーをストップします。");
					p.sendMessage(pf + ChatColor.GREEN +"/sbt help : helpを表示します。");
					p.sendMessage(pf + ChatColor.GREEN +"/sbt reload : configをリロードします。");
					p.sendMessage(pf + ChatColor.GREEN +"*=======================================*");
				}
				if(args[0].equalsIgnoreCase("stop")){
					Bukkit.broadcastMessage(pf +"タイマーをストップしました。");
					Bukkit.getServer().getScheduler().cancelTask(task);
					TestRunnable.timer2.put("タイマ", 1);
				}
				if(args[0].equalsIgnoreCase("start")){
					if(args.length<=1){
						sender.sendMessage(pf + ChatColor.RED +"コマンドに入力されていないところがあります。");
						sender.sendMessage(pf + ChatColor.WHITE +"/sbt start <時間>");
						return false;
					}
					time.put("タイム", 0);
					time.put("タイム", Integer.parseInt(args[1]));
					TestRunnable.timer2.put("タイマ", Integer.parseInt(args[1])+1);
					final int t = time.get("タイム");
					int m = time.get("タイム")/60;
					int s = time.get("タイム")%60;
					ScoreboardManager manager = Bukkit.getScoreboardManager();
					Scoreboard board = manager.getMainScoreboard();
					final Objective objective = board.registerNewObjective("timer", "dummy");
					objective.setDisplaySlot(DisplaySlot.SIDEBAR);
					objective.setDisplayName(ChatColor.AQUA +""+ ChatColor.BOLD +"タイマー");
					Score score = objective.getScore(Bukkit.getOfflinePlayer(ChatColor.GREEN + "時間:"+ ChatColor.YELLOW));
					score.setScore(time.get("タイム"));
					Bukkit.broadcastMessage(pf +"タイマーを開始します。");
					Bukkit.broadcastMessage(pf +"時間は"+ ChatColor.LIGHT_PURPLE +""+ m +"分"+ s +"秒"+ ChatColor.AQUA +"です。");
					for(Player tg : Bukkit.getOnlinePlayers()){
						tg.playSound(tg.getLocation(), Sound.EXPLODE, 1, 15);
					}
					new TestRunnable().runTaskTimer(this, 0, 20);
					task = Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
						int sc = t;
						public void run() {
							if(sc==600){
								Bukkit.broadcastMessage(pf +"残り時間10分です。");
							}
							if(sc==300){
								Bukkit.broadcastMessage(pf +"残り時間5分です。");
							}
							if(sc==180){
								Bukkit.broadcastMessage(pf +"残り時間3分です。");
							}
							if(sc==120){
								Bukkit.broadcastMessage(pf +"残り時間2分です。");
							}
							if(sc==60){
								Bukkit.broadcastMessage(pf +"残り時間1分です。");
							}
							if(sc==30){
								Bukkit.broadcastMessage(pf +"残り時間30秒です。");
							}
							if(sc==15){
								Bukkit.broadcastMessage(pf +"残り時間15秒です。");
							}
							if(sc==10){
								Bukkit.broadcastMessage(pf +"残り時間10秒です。");
								for(Player tg : Bukkit.getOnlinePlayers()){
									tg.playSound(tg.getLocation(), Sound.CLICK, 1, 5);
								}
							}
							if(sc==9){
								Bukkit.broadcastMessage(pf +"残り時間9秒です。");
								for(Player tg : Bukkit.getOnlinePlayers()){
									tg.playSound(tg.getLocation(), Sound.CLICK, 1, 5);
								}
							}
							if(sc==8){
								Bukkit.broadcastMessage(pf +"残り時間8秒です。");
								for(Player tg : Bukkit.getOnlinePlayers()){
									tg.playSound(tg.getLocation(), Sound.CLICK, 1, 5);
								}
							}
							if(sc==7){
								Bukkit.broadcastMessage(pf +"残り時間7秒です。");
								for(Player tg : Bukkit.getOnlinePlayers()){
									tg.playSound(tg.getLocation(), Sound.CLICK, 1, 5);
								}
							}
							if(sc==6){
								Bukkit.broadcastMessage(pf +"残り時間6秒です。");
								for(Player tg : Bukkit.getOnlinePlayers()){
									tg.playSound(tg.getLocation(), Sound.CLICK, 1, 5);
								}
							}
							if(sc==5){
								Bukkit.broadcastMessage(pf +"残り時間5秒です。");
								for(Player tg : Bukkit.getOnlinePlayers()){
									tg.playSound(tg.getLocation(), Sound.CLICK, 1, 5);
								}
							}
							if(sc==4){
								Bukkit.broadcastMessage(pf +"残り時間4秒です。");
								for(Player tg : Bukkit.getOnlinePlayers()){
									tg.playSound(tg.getLocation(), Sound.CLICK, 1, 5);
								}
							}
							if(sc==3){
								Bukkit.broadcastMessage(pf +"残り時間3秒です。");
								for(Player tg : Bukkit.getOnlinePlayers()){
									tg.playSound(tg.getLocation(), Sound.CLICK, 1, 5);
								}
							}
							if(sc==2){
								Bukkit.broadcastMessage(pf +"残り時間2秒です。");
								for(Player tg : Bukkit.getOnlinePlayers()){
									tg.playSound(tg.getLocation(), Sound.CLICK, 1, 5);
								}						for(Player tg : Bukkit.getOnlinePlayers()){
									tg.playSound(tg.getLocation(), Sound.CLICK, 1, 5);
								}
							}
							if(sc==1){
								Bukkit.broadcastMessage(pf +"残り時間1秒です。");
								for(Player tg : Bukkit.getOnlinePlayers()){
									tg.playSound(tg.getLocation(), Sound.CLICK, 1, 5);
								}
							}
							if(sc==0){
								Bukkit.broadcastMessage(pf +"試合終了です！");
								Bukkit.broadcastMessage(pf +"タイマーを終了します。");
								getServer().dispatchCommand(getServer().getConsoleSender(), getConfig().getString("Commands.set1"));
								getServer().dispatchCommand(getServer().getConsoleSender(), getConfig().getString("Commands.set2"));
								getServer().dispatchCommand(getServer().getConsoleSender(), getConfig().getString("Commands.set3"));
								getServer().dispatchCommand(getServer().getConsoleSender(), getConfig().getString("Commands.set4"));
								getServer().dispatchCommand(getServer().getConsoleSender(), getConfig().getString("Commands.set5"));
								for(Player tg : Bukkit.getOnlinePlayers()){
									tg.playSound(tg.getLocation(), Sound.LEVEL_UP, 1, 2);
									Location loc = tg.getLocation();
									loc.setX(getConfig().getInt("tppoint.x"));
									loc.setY(getConfig().getInt("tppoint.y"));
									loc.setZ(getConfig().getInt("tppoint.z"));
									tg.teleport(loc);
								}
								Bukkit.getServer().getScheduler().cancelTask(task);
							}
							sc--;
						}
					}, 0, 20);
				}
			}
		}
		return false;
	}
}